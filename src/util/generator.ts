import axios from "axios";

const BASE_URL = "https://www.wiris.net/demo/editor/render";

export interface EquationData {
  equation: string;
}

export interface EquationOutput {
  error?: string;
  svg?: string;
}

export default class EquationGenerator {
  generate = async (data: EquationData): Promise<EquationOutput> => {
    try {
      const response = await axios.get(
        `${BASE_URL}?format=svg&latex=${encodeURIComponent(data.equation)}`
      );

      console.log(response);

      return {
        svg: response.data,
      };
    } catch (error) {
      console.error(error);
      return {
        error,
      };
    }
  };
}
